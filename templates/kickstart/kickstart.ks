# {{ ansible_managed }}

text

lang {{ lang }}.UTF-8
keyboard {{ keyboard }}

network --onboot yes --bootproto {{ network.bootproto }}{% if network.bootproto == 'static' %} --ip {{ network.ip | ipaddr('address') }} --netmask {{ network.ip | ipaddr('netmask') }}{% for n in network.nameservers %} --nameserver {{ n }}{% endfor %} --gateway {{ network.gateway }}{% endif %} --noipv6 --hostname {{ name }}


timezone --utc {{ timezone }}
rootpw --iscrypted {{ root_pw |password_hash('sha512') }}
{% if sshkey is defined %}
{% if distribution == 'Fedora' %}
sshkey --username=root "{{ sshkey }}"
{% endif %}
{% endif %}
authconfig --enableshadow --passalgo=sha512 --enablefingerprint

bootloader --location=mbr --driveorder=vda

zerombr
clearpart --all --drive=vda
part /boot --fstype={{ filesystem }} --size=500 --asprimary
part pv.01 --grow --size=5000
volgroup vg_root_{{ name }} pv.01
logvol / --vgname=vg_root_{{ name }} --fstype={{ filesystem }} --grow --size=5000 --maxsize=8000 --name=root

services --enabled=sshd
firewall --service=ssh

reboot

%packages

openssh-server
python
{% if extra_packages is defined %}
{% for p in extra_packages %}
{{ p }}
{% endfor %}
{% endif %}
%end

%post --log=/root/ansible_post.log
{% if sshkey is defined %}
{% if distribution == 'Centos' %}
mkdir -p /root/.ssh/
chmod 700 /root/.ssh/
echo "{{ sshkey }}" >> /root/.ssh/authorized_keys
restorecon -Rv /root/.ssh/
{% endif %}
config="/etc/ssh/sshd_config"
if grep -q '^PermitRootLogin ' $config; then
    sed 's/^PermitRootLogin .*/PermitRootLogin without-password/' -i $config
else
    echo "PermitRootLogin without-password" >> $config
fi;

{% endif %}

{% if postinstall is defined %}
{{ postinstall }}
{% endif %}
%end
