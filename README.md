This role enable to make sure a VM is installed on a specified
virtualisation hosts.


# Example

One example of playbook to do that:

```
- hosts: virt01.example.org
  roles:
  - role: guest_virt_install
    name: guest.example.org
    disk_size: 8
    volgroup: volume01
    mem_size: 2048
    num_cpus: 2
    distribution: Fedora
    version: 22
    bridge: virbr0
```

For now, this is a very basic wrapper around virt-install, but more options
are gonna be added.

# Limitation

Beware, the system only make sure that the VM is installed and by default start it, but will
not make sure the VM is started if the admin decide to stop it. This is a difference with the
service module.
